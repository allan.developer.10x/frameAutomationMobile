package br.com.mobile;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * @author Allan Caetano
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        plugin = {
                "pretty",
                "json:target/cucumber.json",
                "json:target/cucumber-report/cucumber.json",
                "br.com.mobile.config.TestListener"
        },
        features = "src/test/resources/features",
        glue = {
                "br.com.mobile.config",
                "br.com.mobile.commons",
                "br.com.mobile.steps",
        },
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        tags = "@ct-001")
public class RunTest  {
}