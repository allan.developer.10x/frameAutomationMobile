package br.com.mobile.mobile.screens.ios;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class LoginIosScreen {

    public LoginIosScreen(IOSDriver driver) {
           PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(20)), this);
    }

    @AndroidFindBy(xpath = "Faça seu login")
    private IOSElement acessarLogin;

    public IOSElement getAcessarLogin() {
        return acessarLogin;
    }

    @AndroidFindBy(className = "android.widget.Button")
    private IOSElement botaoAcessarLogin;

    public IOSElement getBotaoAcessarLogin() {
        return botaoAcessarLogin;
    }

    @AndroidFindBy(xpath = "Sigla de Rede")
    private IOSElement digitarLogin;

}
