package br.com.mobile.mobile.screens.android;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class LoginAndroidScreen {

    public LoginAndroidScreen(AndroidDriver driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(20)), this);
    }

    @AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc='Faça seu login ']")
    private AndroidElement acessarLogin;

    public AndroidElement getAcessarLogin() {
        return acessarLogin;
    }

    @AndroidFindBy(className = "android.widget.Button")
    private AndroidElement botaoAcessarLogin;

    public AndroidElement getBotaoAcessarLogin() {
        return botaoAcessarLogin;
    }

    @AndroidFindBy(xpath = "//android.view.View[@content-desc=\"Selecione sua empresa\"]/android.widget.EditText[1]")
    private AndroidElement digitarLogin;

    public AndroidElement getDigitarLogin() {
        return digitarLogin;
    }

    @AndroidFindBy(xpath = "//android.view.View[@content-desc=\"Selecione sua empresa\"]/android.widget.EditText[1]")
    private AndroidElement campoSiglaRede;

    public AndroidElement getCampoSiglaRede() {
        return campoSiglaRede;
    }

    @AndroidFindBy(accessibility = "Não")
    private AndroidElement recusarBiometria;

    public AndroidElement getRecusarBiometria() {
        return recusarBiometria;
    }

    @AndroidFindBy(accessibility = "Olá, Guilherme!")
    private AndroidElement usuarioLogado;

    public AndroidElement getUsuarioLogado() {
        return usuarioLogado;
    }

    @AndroidFindBy(accessibility = "Bem-vindo ao É Comigo")
    private AndroidElement telaBemVindo;

    public AndroidElement getTelaBemVindo() {
        return telaBemVindo;
    }

    @AndroidFindBy(accessibility = "Banco Santander Brasil")
    private AndroidElement selecionarEmpresaSantander;

    public AndroidElement getSelecionarEmpresaSantander() {
        return selecionarEmpresaSantander;
    }

    @AndroidFindBy(xpath = "//android.view.View[@content-desc='Selecione sua empresa']/android.widget.EditText[1]")
    private AndroidElement campoSiglaDeRede;

    public AndroidElement getCampoSiglaDeRede() {
        return campoSiglaDeRede;
    }

    @AndroidFindBy(xpath = "//android.view.View[@content-desc='Selecione sua empresa']/android.widget.EditText[2]")
    private AndroidElement campoSenha;

    public AndroidElement getCampoSenha() {
        return campoSenha;
    }

    @AndroidFindBy(xpath = "//android.widget.Button[@content-desc='Não']")
    private AndroidElement botaoNao;

    public AndroidElement getBotaoNao() {
        return botaoNao;
    }

    @AndroidFindBy(xpath = "//android.widget.Button[@content-desc='Sim']")
    private AndroidElement botaoSim;

    public AndroidElement getBotaoSim() {
        return botaoSim;
    }
}
