package br.com.mobile.mobile.funcs.evidence;

import java.util.Map;

import br.com.mobile.commons.BaseTest;
import io.cucumber.datatable.DataTable;

public class Parameters extends BaseTest {

    public void set(DataTable parametros) {
        for (Map<Object, Object> map : parametros.asMaps(String.class, String.class)) {
            evidence.setIdTeste((String) map.get("Teste ID"));
            evidence.setNomeTeste(fixIssues((String) map.get("Teste")));
            evidence.setCicloTeste((String) map.get("Sprint"));
            evidence.setExecutorTeste((String) map.get("Executor"));
        }
    }

    private String fixIssues(String str) {
        return str
                .replace("\\", "")
                .replace("/", "")
                .replace("!", "")
                .replace("@", "")
                .replace("#", "")
                .replace("$", "")
                .replace("%", "")
                .replace("&", "")
                .replace("*", "")
                .replace("(", "")
                .replace(")", "")
                .replace("[", "")
                .replace("]", "")
                .replace("+", "")
                .replace("=", "")
                .replace("£", "")
                .replace("¢", "")
                .replace("§", "");
    }
}

