package br.com.mobile.mobile.funcs;

import br.com.mobile.commons.BaseTest;
import br.com.mobile.commons.lang.DSL;
import br.com.mobile.mobile.screens.android.LoginAndroidScreen;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import static org.junit.Assert.*;

public class CommonFuncs extends BaseTest {

    private DSL dsl = new DSL(driver);
    private LoginAndroidScreen loginAndroidScreen = new LoginAndroidScreen((AndroidDriver) driver);
    private TouchAction touchAction = new TouchAction(driver);


    public void visualizoATela(String tela) {
        // TODO: 09/03/22
    }

    public void apertarTeclaAndroid(String tecla) {
        switch (tecla) {
            case "Enter":
                dsl.apertarTecla(AndroidKey.ENTER);
                break;
            default:
                throw new IllegalArgumentException("A tecla especificada não existe.");
        }
    }

    public void visualizarMensagemAndroid(String mensagem) {
        switch (mensagem) {
            case "Salvo com sucesso!":
                assertTrue(
                        "A mensagem 'Salvo com sucesso!' não estava visível.", true
                        // TODO: 09/03/22
                );
                break;
            default:
                throw new IllegalArgumentException("A mensagem especificada não existe.");
        }
    }


}
