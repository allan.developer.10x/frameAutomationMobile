package br.com.mobile.mobile.funcs.android;

import br.com.mobile.commons.BaseTest;
import br.com.mobile.commons.lang.DSL;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;

import static io.appium.java_client.touch.offset.PointOption.point;

public class HomeFunc extends BaseTest {

    private DSL dsl = new DSL(driver);
    TouchAction touchAction = new TouchAction(driver);


    public void deslizarAteAtalho(String atalhos) throws InterruptedException {
        switch (atalhos){
            case "MOVER PELA TELA":
                Thread.sleep(10000);
                touchAction
                        .longPress(point(684, 1112))
                        .moveTo(point(115, 1112))
                        .release()
                        .perform();
                Thread.sleep(800);

                break;
        }
    }


}
