package br.com.mobile.mobile.steps.ios;

import br.com.mobile.mobile.funcs.LoginIosFunc;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class LoginIosStep {

    private LoginIosFunc loginIosFunc = new LoginIosFunc();

    @E("acesso o App IOS")
    public void acessoOAppIOS() throws InterruptedException {
        Thread.sleep(4000);
        loginIosFunc.acessarLogin();
    }

}
