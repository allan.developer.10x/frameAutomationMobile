package br.com.mobile.mobile.steps.android;

import br.com.mobile.mobile.funcs.LoginAndroidFunc;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class LoginAndroidStep {

    private LoginAndroidFunc loginAndroidFunc = new LoginAndroidFunc();

    @E("acesso o App")
    public void acessoOApp() throws InterruptedException {
        loginAndroidFunc.acessarLogin();
    }

}
