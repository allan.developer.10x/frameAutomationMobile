package br.com.mobile.mobile.steps;

import br.com.mobile.mobile.funcs.CommonFuncs;
import io.cucumber.java.pt.*;
import io.cucumber.datatable.DataTable;
import br.com.mobile.mobile.funcs.evidence.Parameters;

public class CommonSteps {

    private CommonFuncs commonFunc = new CommonFuncs();
    private Parameters parameters = new Parameters();

    @Dado("que estou executando o teste")
    public void queEstouExecutandoOTeste(DataTable parametros) {
        parameters.set(parametros);
    }

    @E("estou executando o teste")
    public void estouExecutandoOTeste(DataTable parametros) {
        parameters.set(parametros);
    }

    @E("clico no botão {string}")
    public void clicoNoBotão(String botao) {
      //  commonFunc.clicarNoBotao(botao);
    }

}

