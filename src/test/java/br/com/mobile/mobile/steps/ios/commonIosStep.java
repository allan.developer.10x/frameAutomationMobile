package br.com.mobile.mobile.steps.ios;

import br.com.mobile.mobile.funcs.evidence.Parameters;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.pt.Dado;

public class commonIosStep {
    private Parameters parameters = new Parameters();

    @Dado("que estou executando o teste no IOS")
    public void queEstouExecutandoOTesteNoIOS(DataTable parametros) {
        parameters.set(parametros);
    }


}
