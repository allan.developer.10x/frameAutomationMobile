#language: pt
#encoding: UTF-8
#author: Yaman Tecnologia LTDA

@mobile @evidence @android
Funcionalidade: Validar testes no App

  @loginAndroid @ct-001 @sprint1 @allan
  Cenario: Validar acesso a tela inicial do App
    Dado que estou executando o teste
      | Teste ID | Teste                                | Executor      | Sprint   |
      | CT-001   | Validar acesso a tela inicial do App | Allan-Caetano | Sprint 1 |
    Quando acesso o App
    Entao visualizo a "Bem-Vindo ao É Comigo"

