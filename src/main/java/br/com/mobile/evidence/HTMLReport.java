package br.com.mobile.evidence;

import java.io.File;
import java.util.List;
import java.util.ArrayList;
import com.rajatthareja.reportbuilder.Color;
import com.rajatthareja.reportbuilder.ReportBuilder;

public class HTMLReport {

    public static void main(String[] args) {
        ReportBuilder reportBuilder = new ReportBuilder();
        String path = System.getProperty("user.dir");
        reportBuilder.setReportDirectory(path + "//target//");
        reportBuilder.setReportFileName("CucumberHTML");
        reportBuilder.setReportTitle("Execução de testes Android & IOS");
        reportBuilder.setReportColor(Color.PURPLE);
        reportBuilder.enableVoiceControl();
        reportBuilder.setAdditionalInfo("Environment", "My Environment");
        List<Object> cucumberJsonReports = new ArrayList<>();
        cucumberJsonReports.add(new File(path + "//target//cucumber.json"));
        cucumberJsonReports.add(new File(path + "//target//"));
//        cucumberJsonReports.add(new URL("http://myReportUrl/report.json"));
        reportBuilder.build(cucumberJsonReports);
    }
}
