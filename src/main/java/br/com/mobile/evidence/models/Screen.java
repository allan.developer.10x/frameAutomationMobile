package br.com.mobile.evidence.models;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import javax.imageio.ImageIO;
import javax.annotation.Nonnull;

import com.itextpdf.text.pdf.codec.Base64;

/**
 * Classe modelo para manipulação das capturas de tela durante execução dos testes.
 */
public class Screen {
    public Screen(@Nonnull String imageString) throws IOException {
        this.imageString = imageString;
        this.image = toImage(imageString);
    }

    public Screen(@Nonnull Image image) throws IOException {
        this.image = image;
        this.imageString = toImageString(image);
    }

    public Screen(@Nonnull Image image, @Nonnull String imageString) {
        this.image = image;
        this.imageString = imageString;
    }

    private Image image;
    private String imageString;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public byte[] toByteArray() {
        return toByteArray(imageString);
    }

    public byte[] toByteArray(String imageString) {
        return Base64.decode(imageString);
    }

    private Image toImage(String imageString) throws IOException {
        return ImageIO.read(new ByteArrayInputStream(toByteArray(imageString)));
    }

    private String toImageString(Image image) throws IOException {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        ImageIO.write(ImageIO.read(ImageIO.createImageInputStream(image)), "png", byteArrayOS);
        return Base64.encodeBytes(byteArrayOS.toByteArray());
    }
}

