package br.com.mobile.evidence.models;

import java.util.Date;
import java.util.List;
import javax.annotation.Nonnull;

/**
 * Classe modelo para manipulação dos dados das evidências.
 */
public class Evidence {

    public Evidence() {
    }

    public Evidence(@Nonnull Evidence evidence) {
        this.idTeste = evidence.idTeste;
        this.nomeTeste = evidence.nomeTeste;
        this.cicloTeste = evidence.cicloTeste;
        this.dataHoraTeste = evidence.dataHoraTeste;
        this.executorTeste = evidence.executorTeste;
        this.tempoExecucao = evidence.tempoExecucao;
        this.situacaoTeste = evidence.situacaoTeste;
        this.erroTeste = evidence.erroTeste;
        this.listaCapturaDeTela = evidence.listaCapturaDeTela;
    }

    public Evidence(String idTeste, String nomeTeste, String cicloTeste, Date dataHoraTeste, String executorTeste, String tempoExecucao, String situacaoTeste, Throwable erro, List<Screen> listaCapturaDeTela) {
        this.idTeste = idTeste;
        this.nomeTeste = nomeTeste;
        this.cicloTeste = cicloTeste;
        this.dataHoraTeste = dataHoraTeste;
        this.executorTeste = executorTeste;
        this.tempoExecucao = tempoExecucao;
        this.situacaoTeste = situacaoTeste;
        this.erroTeste = erro;
        this.listaCapturaDeTela = listaCapturaDeTela;
    }

    private String idTeste;
    private String nomeTeste;
    private String cicloTeste;
    private Date dataHoraTeste;
    private String executorTeste;
    private String tempoExecucao;
    private String situacaoTeste;
    private Throwable erroTeste;
    private List<Screen> listaCapturaDeTela;

    public String getIdTeste() {
        return idTeste;
    }

    public Evidence setIdTeste(String idTeste) {
        this.idTeste = idTeste;
        return this;
    }

    public String getNomeTeste() {
        return nomeTeste;
    }

    public Evidence setNomeTeste(String nomeTeste) {
        this.nomeTeste = nomeTeste;
        return this;
    }

    public String getCicloTeste() {
        return cicloTeste;
    }

    public Evidence setCicloTeste(String cicloTeste) {
        this.cicloTeste = cicloTeste;
        return this;
    }

    public Date getDataHoraTeste() {
        return dataHoraTeste;
    }

    public Evidence setDataHoraTeste(Date dataHoraTeste) {
        this.dataHoraTeste = dataHoraTeste;
        return this;
    }

    public String getExecutorTeste() {
        return executorTeste;
    }

    public Evidence setExecutorTeste(String executorTeste) {
        this.executorTeste = executorTeste;
        return this;
    }

    public String getTempoExecucao() {
        return tempoExecucao;
    }

    public Evidence setTempoExecucao(String tempoExecucao) {
        this.tempoExecucao = tempoExecucao;
        return this;
    }

    public String getSituacaoTeste() {
        return situacaoTeste;
    }

    public Evidence setSituacaoTeste(String situacaoTeste) {
        this.situacaoTeste = situacaoTeste;
        return this;
    }

    public List<Screen> getListaCapturaDeTela() {
        return listaCapturaDeTela;
    }

    public Evidence setListaCapturaDeTela(List<Screen> listaCapturaDeTela) {
        this.listaCapturaDeTela = listaCapturaDeTela;
        return this;
    }

    public Throwable getErroTeste() {
        return erroTeste;
    }

    public Evidence setErroTeste(Throwable erroTeste) {
        this.erroTeste = erroTeste;
        return this;
    }
}
