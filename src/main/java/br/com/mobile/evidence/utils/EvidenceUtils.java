package br.com.mobile.evidence.utils;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import br.com.mobile.evidence.models.Screen;
import br.com.mobile.evidence.models.Evidence;
import static br.com.mobile.commons.utils.DateUtils.*;

public class EvidenceUtils {
    private static final String userDir = System.getProperty("user.dir");
    private static final String fSeparator = System.getProperty("file.separator");
    public static final String EVIDENCE_FOLDER_PATH = userDir + fSeparator + "evidence" + fSeparator;
    public static final String PASSED_FOLDER_PATH = EVIDENCE_FOLDER_PATH + fSeparator + "Passed" + fSeparator;
    public static final String FAILED_FOLDER_PATH = EVIDENCE_FOLDER_PATH + fSeparator + "Failed" + fSeparator;

    public static String getScreenshotAsBase64(WebDriver webDriver) {
        return ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BASE64);
    }

    public static Screen getScreenshot(WebDriver webDriver) throws IOException {
        return new Screen(getScreenshotAsBase64(webDriver));
    }

    public static String getEvidenceFileName(Evidence evidence) {
        return evidence.getIdTeste() + "-" + formatarData(obterDataAtual(), DATA) + "_" + formatarData(obterDataAtual(), HORA) + ".pdf";
    }
}
