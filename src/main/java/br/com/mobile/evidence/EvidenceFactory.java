package br.com.mobile.evidence;

import java.io.*;

import br.com.mobile.evidence.models.Evidence;
import br.com.mobile.evidence.models.Screen;
import br.com.mobile.evidence.utils.EvidenceUtils;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import static br.com.mobile.commons.utils.FileUtils.*;
import static br.com.mobile.commons.utils.DateUtils.*;

/**
 * * Classe construra de evidências
 */
public class EvidenceFactory {

    private static Document document;

    public static void build(Evidence evidence) throws DocumentException, IOException {
        document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
        PdfWriter.getInstance(document, new FileOutputStream(createEvidenceDirectory(evidence)));
        document.open();
        insertScreenshots(evidence);
        document.close();
    }

    private static String createEvidenceDirectory(Evidence evidence) {
        String pathName;
        if (evidence.getSituacaoTeste().equalsIgnoreCase("passou")) {
            if (createDirectoryIfNotExists(EvidenceUtils.PASSED_FOLDER_PATH)) {
                pathName = EvidenceUtils.PASSED_FOLDER_PATH + EvidenceUtils.getEvidenceFileName(evidence);
            } else throw new RuntimeException("Não foi possível criar o diretório de evidências.");
        } else if (evidence.getSituacaoTeste().equalsIgnoreCase("falhou")) {
            if (createDirectoryIfNotExists(EvidenceUtils.FAILED_FOLDER_PATH)) {
                pathName = EvidenceUtils.FAILED_FOLDER_PATH + EvidenceUtils.getEvidenceFileName(evidence);
            } else throw new RuntimeException("Não foi possível criar o diretório de evidências.");
        } else throw new RuntimeException("O Status " + evidence.getSituacaoTeste() + " não foi definido.");
        return pathName;
    }

    private static void insertHeader(Evidence evidence) throws DocumentException, IOException {
        Image image = Image.getInstance(EvidenceUtils.class + fileSeparator + "img.png");
        image.scaleToFit(PageSize.A4);
        image.setAlignment(Element.ALIGN_CENTER);
        image.scaleAbsolute(580,60);
        document.add(image);
        PdfPTable table1 = new PdfPTable(new float[]{15f, 85f}),
                table2 = new PdfPTable(new float[]{15f, 35f, 15f, 35f});
        PdfPCell testIdCell = new PdfPCell(getPhrase("Número")),
                testNameCell = new PdfPCell(getPhrase("Teste")), cycleCell = new PdfPCell(getPhrase("Ciclo")),
                dateCell = new PdfPCell(getPhrase("Data")), testerCell = new PdfPCell(getPhrase("Executor")),
                timeCell = new PdfPCell(getPhrase("Duração")), statusCell = new PdfPCell(getPhrase("Status"));
        table1.addCell(testNameCell);
        table1.addCell(new PdfPCell(new Phrase(evidence.getNomeTeste())));
        table2.addCell(testIdCell);
        table2.addCell(new PdfPCell(new Phrase(evidence.getIdTeste())));
        table2.addCell(cycleCell);
        table2.addCell(new PdfPCell(new Phrase(evidence.getCicloTeste())));
        table2.addCell(dateCell);
        table2.addCell(new PdfPCell(new Phrase(formatarData(evidence.getDataHoraTeste(), DATA_HORA_MOLDADA))));
        table2.addCell(testerCell);
        table2.addCell(new PdfPCell(new Phrase(evidence.getExecutorTeste())));
        table2.addCell(timeCell);
        table2.addCell(new PdfPCell(new Phrase(evidence.getTempoExecucao())));
        table2.addCell(statusCell);
        table2.addCell(getStatusCell(evidence.getSituacaoTeste()));
        table1.setSpacingBefore(10f);
        table2.setSpacingAfter(20f);
        document.add(table1);
        document.add(table2);
    }

    private static Phrase getPhrase(String text) {
        Phrase aux = new Phrase(), phrase = new Phrase(text, new Font(aux.getFont().getFamily(), aux.getFont().getSize(), Font.BOLD));
        return phrase;
    }

    private static Paragraph getParagraph(String text) {
        Paragraph aux = new Paragraph(), paragraph = new Paragraph(text, new Font(aux.getFont().getFamily(), aux.getFont().getSize(), Font.BOLD));
        return paragraph;
    }

    private static Paragraph getParagraph(String text, float size, int style, BaseColor color) {
        Paragraph aux = new Paragraph(), paragraph = new Paragraph(text, new Font(aux.getFont().getFamily(), size, style, color));
        return paragraph;
    }

    private static PdfPCell getStatusCell(String status) {
        Phrase phrase, aux = new Phrase();
        if (status.equals("Passou")) {
            phrase = new Phrase(status, new Font(aux.getFont().getFamily(), aux.getFont().getSize(), Font.BOLD, BaseColor.GREEN));
        } else if (status.equals("Falhou")) {
            phrase = new Phrase(status, new Font(aux.getFont().getFamily(), aux.getFont().getSize(), Font.BOLD, BaseColor.RED));
        } else {
            phrase = new Phrase(status, new Font(aux.getFont().getFamily(), aux.getFont().getSize(), Font.BOLD));
        }
        return new PdfPCell(phrase);
    }

    private static void insertScreenshots(Evidence evidence) throws IOException, DocumentException {
        Image image;
        if (evidence.getListaCapturaDeTela() != null && evidence.getListaCapturaDeTela().size() > 0) {
            for (Screen screen : evidence.getListaCapturaDeTela()) {
                image = Image.getInstance(screen.toByteArray());
                image.scaleToFit(PageSize.A4.getWidth() - (PageSize.A4.getWidth() / 4), PageSize.A4.getHeight() - (PageSize.A4.getHeight() / 4));
                image.setAlignment(Image.ALIGN_CENTER);
                insertHeader(evidence);
                document.add(image);
                document.newPage();
            }
            if (evidence.getErroTeste() != null) {
                insertErrorPage(evidence);
            }
        } else
            System.out.println("Lista de evidências nula ou vazia.");
    }

    private static void insertErrorPage(Evidence evidence) throws DocumentException {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        Paragraph paragraph = getParagraph("Detalhes do erro:");
        evidence.getErroTeste().printStackTrace(pw);
        paragraph.setSpacingBefore(40f);
        paragraph.setSpacingAfter(20f);
        document.add(paragraph);
        paragraph = getParagraph(sw.toString(), 10.5f, 0, BaseColor.RED);
        document.add(paragraph);
        pw.close();
        try {
            sw.close();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }
}
