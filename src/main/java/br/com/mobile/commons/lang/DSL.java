package br.com.mobile.commons.lang;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;


/**
 * <p>The domain specific language class.</p>
 * <p>Abstracts commands in a more natural and understandable language.</p>
 *
 * @author Yaman Tecnologia
 * @version 1.0.0
 */
public class DSL {

    /* Construtores */

    public DSL(MobileDriver driver) {
        mobileDriver = driver;
        webDriverWait = new WebDriverWait(mobileDriver, 30);
    }

    /* Atributos */

    private static MobileDriver mobileDriver;
    private static WebDriverWait webDriverWait;

    /* Cliques */

    public void clicar(AndroidElement element) {
        element.click();
    }

    public void clicar(IOSElement element) {
        element.click();
    }

    public void clicar(AndroidElement elemento, boolean aguardarVisibilidade) {
        if (aguardarVisibilidade) {
            if (!aguardarFicarVisivel(elemento, 10))
                throw new IllegalArgumentException("O elemento não estava visível!");
        }
        elemento.click();
    }

    public void clicarTexto(String texto) {
        mobileDriver.findElementByXPath("//*[@text='" + texto + "']").click();
    }

    public void clicarSeEstiverVisivel(AndroidElement elemento) {
        if (aguardarFicarVisivel(elemento, 10)) {
            clicar(elemento);
        }
    }

    /* Escritas */

    public void escrever(AndroidElement element, String text) {
        element.sendKeys(text);
    }

    public void escrever(AndroidElement elemento, String texto, boolean aguardarVisibilidade) {
        if (aguardarVisibilidade) {
            if (!aguardarFicarVisivel(elemento, 10))
                throw new IllegalArgumentException("O elemento não estava visível!");
        }
        elemento.sendKeys(texto);
    }

    public void escreverComTeclado(String texto) {
        ((AndroidDriver) mobileDriver).getKeyboard().sendKeys(texto);
    }

    public void escreverComTeclado(AndroidElement elemento, String texto, boolean aguardarVisibilidade) {
        if (aguardarVisibilidade) {
            if (!aguardarFicarVisivel(elemento, 10))
                throw new IllegalArgumentException("O elemento não estava visível!");
        }
        escreverComTeclado(texto);
    }

    /* Limpar */

    /* Visibilidade e Presença */

    public boolean estaNaTela(AndroidElement element) {
        boolean visivel;
        visivel = element.isDisplayed();
        return visivel;
    }

    public boolean estaVisivel(AndroidElement element) {
        boolean estavisivel;
        try {
            estavisivel = webDriverWait.until(ExpectedConditions.visibilityOf(element)).isDisplayed();
        } catch (TimeoutException exception) {
            estavisivel = false;
        }
        return estavisivel;
    }

    public boolean aguardarFicarVisivel(AndroidElement elemento, long segundos) {
        boolean estavisivel;
        try {
            estavisivel = new WebDriverWait(mobileDriver, segundos)
                    .until(ExpectedConditions.visibilityOf(elemento)).isDisplayed();
        } catch (TimeoutException exception) {
            estavisivel = false;
        }
        return estavisivel;
    }

    /* Elementos */

    public String obterTexto(AndroidElement elemento) {
        return elemento.getText();
    }

    public boolean contemOValor(AndroidElement elemento, String valor) {
        return elemento.getText().equals(valor);
    }

    public void tocarEm(int x, int y) {
        new TouchAction(mobileDriver)
                .tap(PointOption.point(x, y))
                .release()
                .perform();
    }

    public void tocarEm(AndroidElement elemento) {
        tocarEm(elemento.getCenter().getX(), elemento.getCenter().getY());
    }

    public void tocarEm(AndroidElement elemento, boolean aguardarVisibilidade) {
        if (aguardarVisibilidade) {
            if (!aguardarFicarVisivel(elemento, 15))
                throw new NoSuchElementException("O elemento não foi encontrado.");
        }
        tocarEm(elemento.getCenter().getX(), elemento.getCenter().getY());
    }

    public AndroidElement obterElementoPorAcessibilidade(String identificador) {
        return (AndroidElement) mobileDriver.findElementByAccessibilityId(identificador);
    }

    public void aguardarPor(long segundos) {
        try {
            new WebDriverWait(mobileDriver, segundos)
                    .until(ExpectedConditions
                                    .visibilityOfElementLocated(By
                                            .xpath("/n@#$/./aAS@FdFD56//..")
                                    )
                    ).isDisplayed();
        } catch (TimeoutException exception) {
        }
    }

    public void tempo() throws InterruptedException {
        Thread.sleep(5000);
    }

    public void deslizarHorizontalmente(int xDe, int xPara, int y) {
        new TouchAction<>(mobileDriver)
                .press(PointOption.point(xDe, y))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(xPara, y))
                .release()
                .perform();
    }

    public void deslizarVerticalmente(int x, int yDe, int yPara) {
        new TouchAction<>(mobileDriver)
                .press(PointOption.point(x, yDe))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(x, yPara))
                .release()
                .perform();
    }

    public void ocultarTeclado() {
        mobileDriver.hideKeyboard();
    }

    public void apertarTecla(AndroidKey androidKey) {
        ((AndroidDriver) mobileDriver).pressKey(new KeyEvent(androidKey));
    }

    public void deslizarVerticalmenteAteFicarVisivel(int tentativas, AndroidElement elemento) {
        int t = 0, size = mobileDriver.manage().window().getSize().getHeight();
        while (!aguardarFicarVisivel(elemento, 5) && t < tentativas) {
            deslizarVerticalmente(
                    mobileDriver.manage().window().getSize().getWidth() / 2,
                    size - ((int) (size * 0.1)),
                    size - ((int) (size * 0.4))
            );
        }
    }

    public void deslizarVerticalmenteAteFicarVisivel(int tentativas, AndroidElement elemento, float pDe, float pPara) {
        int t = 0, size = mobileDriver.manage().window().getSize().getHeight();
        while (!aguardarFicarVisivel(elemento, 5) && t < tentativas) {
            deslizarVerticalmente(
                    mobileDriver.manage().window().getSize().getWidth() / 2,
                    size - ((int) (size * pDe)),
                    size - ((int) (size * pPara))
            );
        }
    }
}

