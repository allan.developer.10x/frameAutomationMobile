package br.com.mobile.commons;

import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import br.com.mobile.commons.exceptions.EvidenceException;
import br.com.mobile.evidence.models.Evidence;
import br.com.mobile.evidence.models.Screen;
import br.com.mobile.evidence.utils.EvidenceUtils;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;

import static br.com.mobile.commons.DriverFactory.*;
import static br.com.mobile.commons.utils.DateUtils.*;

public class BaseTest {

    protected static long initialTime;
    protected static Evidence evidence;
    private static List<Screen> screenList;
    protected static MobileDriver<MobileElement> driver;

    protected final void initializeMobile(String platform) throws IOException {
        driver = getDriver(platform);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    protected final void initializeEvidence() {
        evidence = new Evidence();
        screenList = new ArrayList<>();
        evidence.setDataHoraTeste(obterDataAtual());
        initialTime = System.currentTimeMillis();
    }

    protected final void finalizeMobile() {
        if (driver != null) {
            driver.quit();
        }
    }
    protected final void finalizeEvidence() {
        evidence.setTempoExecucao(((System.currentTimeMillis() - initialTime) / 10000) + " segundos.");
        evidence.setListaCapturaDeTela(screenList);
    }

    protected final void screenCapture() throws EvidenceException {
        try {
            screenList.add(EvidenceUtils.getScreenshot(driver));
        } catch (IOException e) {
            EvidenceException ex = new EvidenceException(e.getMessage(), e);
            throw ex;
        }
    }

    protected final void freeze(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace(System.err);
        }
    }
}

