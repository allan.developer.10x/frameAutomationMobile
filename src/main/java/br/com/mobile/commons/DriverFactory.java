package br.com.mobile.commons;

import java.io.File;
import java.net.URL;
import java.io.IOException;

import br.com.mobile.commons.utils.FileUtils;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobilePlatform;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverFactory {

    private static final String PROPERTIES_FILE = FileUtils.userDir + FileUtils.fileSeparator + "config" + FileUtils.fileSeparator + "mobile.properties";

    public static MobileDriver<MobileElement> getDriver(String browserName) throws IllegalArgumentException, IOException {
        MobileDriver driver;
        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
      //  desiredCapabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true);
        if (browserName.equalsIgnoreCase("ANDROID")) {
            File appDir = new File("src/main/resources/app");
            File app = new File(appDir, "aquiSeu.apk");
            desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, FileUtils.getProperty(PROPERTIES_FILE, "device.android.name"));
            desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, FileUtils.getProperty(PROPERTIES_FILE, "platform.android.automation.name"));
            desiredCapabilities.setCapability(MobileCapabilityType.NO_RESET, true);
            desiredCapabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
            driver = new AndroidDriver<MobileElement>(url, desiredCapabilities);
        } else if (browserName.equalsIgnoreCase("IOS")) {
            File appDir = new File("src/main/resources/app");
            File appIOs = new File(appDir, "aquiSeu.ipa");
            desiredCapabilities.setCapability(MobileCapabilityType.VERSION, 14.5);
            desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.IOS);
            desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone");
            desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            desiredCapabilities.setCapability(MobileCapabilityType.UDID, FileUtils.getProperty(PROPERTIES_FILE,"udid.ios"));
            desiredCapabilities.setCapability(MobileCapabilityType.NO_RESET, true);
            desiredCapabilities.setCapability(MobileCapabilityType.APP, appIOs.getAbsolutePath());
            driver = new IOSDriver<MobileElement>(url, desiredCapabilities);
        } else throw new IllegalArgumentException("This platform isn't implemented.");
        return driver;
    }
}