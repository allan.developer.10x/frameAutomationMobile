package br.com.mobile.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileUtils {

    public static final String userDir = System.getProperty("user.dir");
    public static final String fileSeparator = System.getProperty("file.separator");

    public static boolean createDirectoryIfNotExists(String path) throws SecurityException {
        File file = new File(path);
        return createDirectoryIfNotExists(file);
    }

    public static boolean createDirectoryIfNotExists(File file) throws SecurityException {
        return file.exists() ? true : file.mkdirs() ? true : false;
    }

    public static String getProperty(String path, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(path));
        return properties.getProperty(key);
    }

}
