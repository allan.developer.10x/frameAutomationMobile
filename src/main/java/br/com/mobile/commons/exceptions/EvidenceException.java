package br.com.mobile.commons.exceptions;

public class EvidenceException extends Exception {

    public EvidenceException() {
        super();
    }

    public EvidenceException(String message) {
        super(message);
    }

    public EvidenceException(Throwable cause) {
        super(cause);
    }

    public EvidenceException(String message, Throwable cause) {
        super(message, cause);
    }
}

