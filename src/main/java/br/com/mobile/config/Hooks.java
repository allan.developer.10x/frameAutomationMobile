package br.com.mobile.config;

import br.com.mobile.commons.BaseTest;
import br.com.mobile.commons.exceptions.EvidenceException;
import io.cucumber.java.*;

import java.io.IOException;

public class Hooks extends BaseTest {

    @Before(value = "@evidence", order = 1)
    public void beforeEvidence() {
        initializeEvidence();
    }

    @Before(value = "@mobile", order = 1)
    public void beforeMobileAnnotation(Scenario scenario) throws IOException {
        if (scenario.getSourceTagNames().contains("@android")) {
            initializeMobile("ANDROID");
        } else if (scenario.getSourceTagNames().contains("@ios")) {
            initializeMobile("IOS");
        } else {
            throw new IllegalArgumentException("O.S. annotation is not specified.");
        }
    }

    @AfterStep
    public void afterStep(Scenario scenario) throws EvidenceException {
        if (scenario.getSourceTagNames().contains("@evidence")) {
            freeze(5);
            screenCapture();
        }
    }

    @After(value = "@mobile", order = 0)
    public void afterMobileAnnotation() {
        finalizeMobile();
    }

    @After(value = "@evidence", order = 0)
    public void afterEvidence() {
        finalizeEvidence();
    }
}

