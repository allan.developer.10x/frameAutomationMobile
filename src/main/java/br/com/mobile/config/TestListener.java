package br.com.mobile.config;

import java.io.IOException;

import br.com.mobile.commons.BaseTest;
import br.com.mobile.evidence.EvidenceFactory;
import com.itextpdf.text.DocumentException;
import io.cucumber.plugin.event.Status;
import io.cucumber.plugin.event.EventPublisher;
import io.cucumber.plugin.event.TestCaseFinished;
import io.cucumber.plugin.ConcurrentEventListener;

public class TestListener extends BaseTest implements ConcurrentEventListener {
    @Override
    public void setEventPublisher(EventPublisher publisher) {
        publisher.registerHandlerFor(TestCaseFinished.class, this::testCaseFinished);
    }

    private void testCaseFinished(TestCaseFinished event) {
        Status status = event.getResult().getStatus();
        Throwable error = event.getResult().getError();
        evidence.setSituacaoTeste(getStatus(status));
        if (error != null) evidence.setErroTeste(error);
        try {
            EvidenceFactory.build(evidence);
        } catch (DocumentException | IOException e) {
            System.err.println("Não foi possível gerar as evidências do teste \"" + event.getTestCase().getName() + "\"");
            e.printStackTrace(System.err);
        }
    }

    private String getStatus(Status status) {
        String name;
        if (status.is(Status.PASSED)) {
            name = "Passou";
        } else if (status.is(Status.FAILED)) {
            name = "Falhou";
        } else if (status.is(Status.SKIPPED)) {
            name = "Ignorado";
        } else if (status.is(Status.AMBIGUOUS)) {
            name = "Ambíguo";
        } else if (status.is(Status.PENDING)) {
            name = "Pendente";
        } else if (status.is(Status.UNDEFINED)) {
            name = "Indefinido";
        } else if (status.is(Status.UNUSED)) {
            name = "Inutilizado";
        } else throw new RuntimeException("Status indefinido.");
        return name;
    }
}

