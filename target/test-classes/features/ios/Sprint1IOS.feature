#language: pt
#encoding: UTF-8
#author: Yaman Tecnologia LTDA

@mobile @evidence @ios
Funcionalidade: Validar testes no App

  @loginIos
  Cenario: Deve acessar Fazer Login no App
    Dado que estou executando o teste no IOS
      | Teste ID | Teste                           | Executor      | Sprint   |
      | CT-001   | Deve acessar Fazer Login no App | Allan-Caetano | Sprint 1 |
    E acesso o App IOS
    E insiro no campo Sigla de rede no aparelho ios "Samsung A10"
    E insiro no campo Senha no aparelho ios "Samsung A10"
    Quando clico no botao "Acessar" ios
    Entao visualizo a "Tela Inicial" ios
